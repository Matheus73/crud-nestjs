import {
  Controller,
  Post,
  Body,
  Get,
  UseGuards,
  Param,
  ValidationPipe,
  Patch,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from 'src/users/dtos/create-user.dto';
import { ReturnUserDto } from 'src/users/dtos/return-user.dto';
import { AuthGuard } from '@nestjs/passport';
import User from 'src/users/user.entity';
import { GetUser } from './get-user.decorator';
import { ChangePasswordDto } from './dtos/change-password.dto';
import { UserRole } from 'src/users/user-roles.enum';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  async signUp(@Body() createUserDto: CreateUserDto): Promise<ReturnUserDto> {
    const user = await this.authService.signUp(createUserDto);
    return { user, message: 'Cadastro realizado com sucesso' };
  }

  @Post('/signin')
  async signIn(
    @Body() createUserDto: CreateUserDto,
  ): Promise<{ token: string }> {
    return await this.authService.signIn(createUserDto);
  }

  @Patch(':token')
  async confirmEmail(
    @Param('token') token: string,
  ): Promise<{ message: string }> {
    const user = await this.authService.confirmEmail(token);
    return { message: 'Email confirmed' };
  }

  @Post('/send-recover-email')
  async sendRecoveryPasswordEmail(
    @Body('email') email: string,
  ): Promise<{ message: string }> {
    await this.authService.sendRecoverPasswordEmail(email);
    return {
      message: 'Foi enviado um email com instruções para resetar sua senha',
    };
  }

  @Patch('/reset-password/:token')
  async resetPassword(
    @Param('token') token: string,
    @Body(ValidationPipe) changePassword: ChangePasswordDto,
  ): Promise<{ message: string }> {
    await this.authService.resetPassword(token, changePassword);
    return { message: 'Senha alterada com sucesso' };
  }

  @Patch(':id/change-password')
  @UseGuards(AuthGuard())
  async changePassword(
    @Param('id') id: string,
    @Body(ValidationPipe) changePassword: ChangePasswordDto,
    @GetUser() user: User,
  ): Promise<{ message: string }> {
    if (user.role !== UserRole.ADMIN && user.id.toString() !== id) {
      throw new UnauthorizedException(
        'Você não tem permissão para realizar esta operação',
      );
    }
    await this.authService.checkPassword(id, changePassword);
    return { message: 'Senha alterada' };
  }

  @Get('/me')
  @UseGuards(AuthGuard())
  getMe(@GetUser() user: User): User {
    return user;
  }
}
