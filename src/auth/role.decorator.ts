import { SetMetadata, CustomDecorator } from '@nestjs/common';

export const Role = (...roles: string[]): CustomDecorator<string> => SetMetadata('role', roles);
