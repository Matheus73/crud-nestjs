import {
  Controller,
  Body,
  Post,
  ValidationPipe,
  Get,
  UseGuards,
  Param,
  Patch,
  ForbiddenException,
  Delete,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { ReturnUserDto } from './dtos/return-user.dto';
import User from './user.entity';
import { AuthGuard } from '@nestjs/passport';
import { Role } from 'src/auth/role.decorator';
import { UserRole } from './user-roles.enum';
import { RolesGuard } from 'src/auth/roles.guard';
import { UpdateUserDto } from './dtos/update-user.dto';
import { GetUser } from 'src/auth/get-user.decorator';
import { FindUsersQueryDto } from './dtos/find-users-query.dto';

@Controller('users')
@UseGuards(AuthGuard(), RolesGuard)
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post()
  @Role(UserRole.ADMIN)
  async createAdminUser(
    @Body(ValidationPipe) createUserDto: CreateUserDto,
  ): Promise<ReturnUserDto> {
    const user = await this.usersService.createAdminUser(createUserDto);
    return {
      user,
      message: 'Administrador cadastrado com sucesso',
    };
  }

  @Get()
  async find(
    @Query() query: FindUsersQueryDto,
  ): Promise<{ users: User[]; total: number }> {
    const users = this.usersService;
    return users.findUsers(query);
  }

  @Get(':id')
  @Role(UserRole.ADMIN)
  async getUserById(@Param('id') id: string): Promise<User> {
    return await this.usersService.findOneById(id);
  }

  @Patch(':id')
  @Role(UserRole.ADMIN)
  async updateUser(
    @Body(ValidationPipe) updateUserDto: UpdateUserDto,
    @GetUser() user: User,
    @Param('id') id: string,
  ): Promise<User> {
    if (user.role != UserRole.ADMIN && user.id != id) {
      throw new ForbiddenException(
        'Você não tem autorização para acessar esse recurso',
      );
    }

    return await this.usersService.updateUser(updateUserDto, id);
  }
  @Delete(':id')
  @Role(UserRole.ADMIN)
  async deleteUser(@Param('id') id: string): Promise<{ message: string }> {
    await this.usersService.deleteUser(id);
    return { message: 'User deleted successfully' };
  }
}
