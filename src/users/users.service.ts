import {
  Injectable,
  UnprocessableEntityException,
  NotFoundException,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import UserRepository from './users.repository';
import { CreateUserDto } from './dtos/create-user.dto';
import User from './user.entity';
import { UserRole } from './user-roles.enum';
import { UpdateUserDto } from './dtos/update-user.dto';
import { FindUsersQueryDto } from './dtos/find-users-query.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async createAdminUser(createUserDto: CreateUserDto): Promise<User> {
    if (createUserDto.password === createUserDto.passwordConfirmation) {
      return this.userRepository.createUser(createUserDto, UserRole.ADMIN);
    }
    throw new UnprocessableEntityException('As senhas não conferem');
  }

  async getAllAdminUser(): Promise<User[]> {
    return await this.userRepository.findAllUser();
  }

  async findOneById(id: string): Promise<User> {
    const user = await this.userRepository.findOne(id, {
      select: ['name', 'email', 'role', 'id', 'status'],
    });

    if (!user) throw new NotFoundException('Usuário não encontrado');

    return user;
  }

  async updateUser(userDto: UpdateUserDto, id: string): Promise<User> {
    const user = await this.findOneById(id);

    const { name, email, role, status } = userDto;

    user.name = name || user.name;
    user.email = email || user.email;
    user.role = role || user.role;
    user.status = status !== undefined ? status : user.status;

    try {
      await user.save();

      return user;
    } catch (error) {
      throw new InternalServerErrorException(
        'Erro ao salvar os dados no banco de dados',
      );
    }
  }

  async deleteUser(id: string): Promise<void> {
    const res = await this.userRepository.delete({ id });
    if (res.affected === 0) {
      throw new NotFoundException(
        'Não foi encontrado um usuário com o ID informado',
      );
    }
  }
  async findUsers(
    queryDto: FindUsersQueryDto,
  ): Promise<{ users: User[]; total: number }> {
    const users = await this.userRepository.findUsers(queryDto);
    return users;
  }
}
